package com.monc;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.google.vr.sdk.audio.GvrAudioEngine;
import com.monc.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {

    private static final String SUCCESS_SOUND_FILE = "audio/finalmusic.ogg";
    private volatile int sourceId = GvrAudioEngine.INVALID_ID;
    private static final float MIN_TARGET_DISTANCE = 3.0f;
    private ActivityMainBinding binding;
    public  float x=0.0f;
    public  float y=0.0f;
    public  float z=MIN_TARGET_DISTANCE;
    public  float vol=0.0f;

    private static final String SUCCESS_SOUND_FILE2 = "audio/sample1.ogg";
    private volatile int sourceId2 = GvrAudioEngine.INVALID_ID;
    private static final float MIN_TARGET_DISTANCE2 = 3.0f;


    GvrAudioEngine gvrAudioEngine;
    Context mainContext;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);
        // Initialize 3D audio engine.

        gvrAudioEngine = new GvrAudioEngine(this, GvrAudioEngine.RenderingMode.BINAURAL_HIGH_QUALITY);
        mainContext = this;
        new Thread(
                new Runnable() {
                    @Override
                    public void run() {
                        gvrAudioEngine.preloadSoundFile(SUCCESS_SOUND_FILE);
                        gvrAudioEngine.preloadSoundFile(SUCCESS_SOUND_FILE2);
                    }
                })
                .start();

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        sourceId2 = gvrAudioEngine.createSoundObject(SUCCESS_SOUND_FILE2);
        sourceId = gvrAudioEngine.createSoundObject(SUCCESS_SOUND_FILE);
        if(sourceId == GvrAudioEngine.INVALID_ID || sourceId2 == GvrAudioEngine.INVALID_ID){
            Toast.makeText(mainContext,"Invalid File",Toast.LENGTH_LONG).show();
        }
        else{
            gvrAudioEngine.setSoundObjectPosition(
                    sourceId, 0.0f, 0.0f, -10.0f);
            gvrAudioEngine.playSound(sourceId, true /* looped playback */);
            gvrAudioEngine.setSoundVolume(sourceId,0.3f);
            Update();
        }

        binding.xminus.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                ManageClicks("xminus");
            } });
        binding.xplus.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                ManageClicks("xplus");
            } });
        binding.yplus.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                ManageClicks("yplus");
            } });
        binding.yminus.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                ManageClicks("yminus");
            } });
        binding.zplus.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                ManageClicks("zplus");
            } });
        binding.zminus.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                ManageClicks("zminus");
            } });


    }

    public  void  ManageClicks(String button){
        switch (button){
            case "xplus":
                if(x<100f){
                    x=x+1f;
                }
                break;
            case "xminus":
                if(x>-100f){
                    x=x-1f;
                }
                break;
            case "yplus":
                if(y<100f){
                    y=y+1f;
                }
                break;
            case "yminus":
                if(y>-100f){
                    y=y-1f;
                }
                break;
            case "zplus":
                if(z<100f){
                    z=z+1f;
                }
                break;
            case "zminus":
                if(z>-100f){
                    z=z-1f;
                }
                break;
        }

        Update();
    }

    public  void  Update(){
       // gvrAudioEngine.pauseSound(sourceId2);
        gvrAudioEngine.setSoundVolume(sourceId2,1);
        gvrAudioEngine.setSoundObjectPosition(
                sourceId2, x, y, z);
        gvrAudioEngine.playSound(sourceId2, true /* looped playback */);
        binding.info.setText("X:"+x+"\nY:"+y+"\nZ:"+z+" \n\n Volume:1");
    }
}